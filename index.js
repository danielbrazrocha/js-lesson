let text =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut suscipit condimentum justo eget volutpat. Morbi felis diam, pellentesque sed aliquam eu, aliquam vitae elit. Integer ut sapien est. Nulla at elit nec nisl dignissim viverra sit amet in libero. Nunc sed bibendum enim. Praesent pharetra semper nulla vitae interdum. Nunc interdum enim imperdiet porta bibendum. Aenean ex ipsum, placerat sed sollicitudin et, tempus non sapien. Suspendisse congue vulputate molestie. Cras auctor vehicula justo tincidunt accumsan. Pellentesque vel iaculis nibh. Aliquam scelerisque eleifend vestibulum. Phasellus non dictum eros. Praesent cursus laoreet ipsum, in porta nisi hendrerit eu. Pellentesque scelerisque felis ut nunc sagittis, quis ultricies nunc euismod. Curabitur quis neque in magna efficitur luctus mollis vel odio. In eu condimentum orci. Curabitur ut ex imperdiet, consectetur diam at, vestibulum risus. Nunc pharetra, est eu placerat dapibus, risus odio blandit ex, at aliquam enim augue sit amet lacus. Cras bibendum, quam non ultrices porttitor, leo urna egestas eros, a sagittis ligula erat vitae purus. In sit amet porta turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae Cras mauris mi, aliquet ac dui non, pellentesque venenatis metus. Integer hendrerit tortor id pharetra ultrices. Suspendisse cursus suscipit congue. Vestibulum ornare faucibus interdum. Aliquam dapibus elit sed lorem laoreet tincidunt. Duis et sem fermentum urna tincidunt rutrum sit amet volutpat elit.";

//Convertendo texto para lowercase
let textLowerCase = text.toLowerCase();

//Iterando a string textLowerCase para criar a variavel newString
let newString = "";
for (let i = 0; i < textLowerCase.length; ++i) {
  let numero = textLowerCase.charCodeAt(i);
  
  //Latin - minusculo
  if (numero >= 65 && numero <= 90) {
    numero -= 64;
    newString += numero;
  }

  //Latin - maisculo
  if (numero >= 97 && numero <= 122) {
    numero -= 96;
    newString += numero;
  }

  //Convertento o caracter "," em -1
  if (numero == 44) {
    numero = -1;
    newString += numero;
  }

  //Convertento o caracter "." em 0
  if (numero == 46) {
    numero = 0;
    newString += numero;
  }
}

//Printando a variável newString com a conversão final
console.log(newString);



/*Comandos de apoio para verificar o código UTF16 de cada caracter a ser alterado (exceções)
let letra = ".";
letra.charCodeAt(letra);
console.log(letra.charCodeAt(letra));
*/


